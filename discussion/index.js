// Array Methods - Javascript ha built in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator Methods
/*
	-	Mutator methods are functions that "mutate" or change an array after they're created.
	-	These methods manipulate tha original array performing various task such as adding and removing elements.
*/


// push()
/*
	- Adds an element inthe end of an array and return the arrays length
	Syntax :
	arrayName.push();

*/

let fruits = ['apple', 'orange', ' kiwi', 'dragonfruit'];
console.log('Current Array:');

console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated Array from push method: ')
console.log(fruits);


// Adding multiple element to an array
fruits.push('Avocado', 'Guava');
console.log("Mutated array from push method");
console.log(fruits);


// pop()

/*
		- Removes the last element in an array AND return the removed element.

		-Syntax : 
		arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method');
console.log(fruits);

// Unshift()
/*
	- Adds one or more elements at the beginning of an array.

	Syntax :
	arrayName.unshift('elementA');
	arrayName.unshift('elementA','elementB');
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method: ');
console.log(fruits);



// Shift
/*
	- removes an element at the beginning of an array and returns the removed element.

	Syntax :
	arrayName.shift();

*/


let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);


// splice()

/*
	- Simultaneuosly removes elements from a specified index number and adds elements

	- Syntax 
	arrayName.splice(startingIndex, deleteCount, elementsTobeAdded);

*/


fruits.splice(1, 1, 'lime', 'cherry')
console.log("Mutated array from splice method");
console.log(fruits);


// sort()

/*
	- Rearrange the array elements in alphanumeric order
	Sytax:

	arrayName.sort();
*/

const randomThings = ['cat', 'boy','apps', 'zoo']


randomThings.sort();
console.log('Mutated array from sort method:');
console.log(randomThings);


// reverse()

/*
	-reverse the order of array elements

	Syntax : 

	arrayName.reverse();

*/

randomThings.reverse();
console.log('Mutated array from reverse method:');
console.log(randomThings);



// - Non-Mutator methods are functions that do not modify or change anarray after ther're created

/*
	- this methods do not manipulate the original array performing various tasks such as returning elements from an array and combining array and printing the output

	

	*indexOf() 

	- returns the index number of the first matching element found in an array 

	- if no match was found, the result will be -1
	- the search process will be done from first element proceeding to the last element.



	- Syntax :
		arrayName.indedxOf(searchvalue);
*/

let countries = ['USA', 'Ph', 'CAN', 'SG', 'Th', 'Br', 'Fr', 'De']

let firstIndex = countries.indexOf('CAN');
console.log('Result of indexOf method: ' + firstIndex);



// slice()

/*
	- portion / slices elements from an array AND returns a new array.
	- Syntax :
		arrayName.slice(startingIndex, );
		arrayName.slice(startingIndex, endingIndex);

*/

// Slicing off elements from a specified index to the firts element

let slicedArrayA = countries.slice(4);
console.log('Result from slice method:')

console.log(slicedArrayA);
console.log(countries);



// Slicing off elements from a specified index to another index
// Note: the last element is not included. 

let slicedArrayB = countries.slice(1, 3);
console.log('Result from sliceB method: ');
console.log(slicedArrayB);




// Slicing of elements starting from the last element of an array

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method: ')
console.log(slicedArrayC);


// forEach()

/*
	- similar to for loop that iterates on earch array element. 
	- For each item in the arra, the anonymous function passed in forEach() method will be run

	- Note: it must have function

	Syntax :
	arrayName.forEach(function(indivElement){
		statement
	});
*/


countries.forEach(function(country) {
	console.log(country);
})


// includes()

/*
	- includes() methjod checks if the argument passed can be found in the array
	- it returns a boolean which can be saved in a variable
	- return true if the argument is found in the array.
	- return false if is not

	- Syntax:
		arrayName.includes(<argumentToFind>)

*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor']; 

let productFound1 = products.includes("Mouse");

        console.log(productFound1);//returns true

        let productFound2 = products.includes("Headset");

        console.log(productFound2);//returns false



        // ======================================



        /*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/






